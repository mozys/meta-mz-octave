SUMMERY = "suitesparse"
DESCRIPTION = "A collection of sparse matrix libraries"
HOMEPAGE = "http://faculty.cse.tamu.edu/davis/suitesparse.html"
LICENSE = "CLOSED"
DEPENDS = "gcc-runtime libgfortran openblas-lapack"

S = "${WORKDIR}/SuiteSparse"
SRC_URI = " \
    http://faculty.cse.tamu.edu/davis/SuiteSparse/SuiteSparse-${PV}.tar.gz \
"

LIC_FILES_CHKSUM = "file://${WORKDIR}/SuiteSparse/README.txt;md5=2ba7f06b19f1dac5cc5929ed2d8972f9"
SRC_URI[md5sum] = "a2926c27f8a5285e4a10265cc68bbc18"
SRC_URI[sha256sum] = "83f4b88657c7dc57681633e8ca6835ddb12c146bc51af77b6494972ed1ea8bc9"

do_configure[noexec] = "1"

do_compile() {
    make -C SuiteSparse_config/xerbla
    make -C SuiteSparse_config 
    for _lib in AMD CAMD COLAMD BTF KLU LDL CCOLAMD UMFPACK CHOLMOD CXSparse \
        SPQR; do make -C ${_lib} library
    done

    mkdir -p shared
    # version numbers can be found in individual changelog files
    # based on:
    # https://archlinuxarm.org/packages/armv7h/suitesparse/files/PKGBUILD

    L=libsuitesparseconfig
    ${LD} --hash-style=gnu -shared -soname ${L}.so.4 -o shared/${L}.so.4.4.4 \
        --whole-archive SuiteSparse_config/${L}.a -lm && \
        ln -sf ${L}.so.4.4.4 shared/${L}.so.4 && \
        ln -sf ${L}.so.4.4.4 shared/${L}.so

    L=libamd
    ${LD} --hash-style=gnu -shared -soname ${L}.so.2 -o shared/${L}.so.2.4.1 \
        --whole-archive AMD/Lib/${L}.a -L./shared -lsuitesparseconfig -lm && \
        ln -sf ${L}.so.2.4.1 shared/${L}.so.2 && \
        ln -sf ${L}.so.2.4.1 shared/${L}.so

    L=libcamd
    ${LD} --hash-style=gnu -shared -soname ${L}.so.2 -o shared/${L}.so.2.4.1 \
        --whole-archive CAMD/Lib/${L}.a -L./shared -lsuitesparseconfig -lm && \
        ln -sf ${L}.so.2.4.1 shared/${L}.so.2 && \
        ln -sf ${L}.so.2.4.1 shared/${L}.so

    L=libcolamd
    ${LD} --hash-style=gnu -shared -soname ${L}.so.2 -o shared/${L}.so.2.9.1 \
        --whole-archive COLAMD/Lib/${L}.a -L./shared -lsuitesparseconfig -lm &&\
        ln -sf ${L}.so.2.9.1 shared/${L}.so.2 && \
        ln -sf ${L}.so.2.9.1 shared/${L}.so

    L=libccolamd
    ${LD} --hash-style=gnu -shared -soname ${L}.so.2 -o shared/${L}.so.2.9.1 \
        --whole-archive CCOLAMD/Lib/${L}.a -L./shared \
        -lsuitesparseconfig -lm && \
        ln -sf ${L}.so.2.9.1 shared/${L}.so.2 && \
        ln -sf ${L}.so.2.9.1 shared/${L}.so

    L=libbtf
    ${LD} --hash-style=gnu -shared -soname ${L}.so.1 -o shared/${L}.so.1.2.1 \
        --whole-archive BTF/Lib/${L}.a && \
        ln -sf ${L}.so.1.2.1 shared/${L}.so.1 && \
        ln -sf ${L}.so.1.2.1 shared/${L}.so

    L=libldl
    ${LD} --hash-style=gnu -shared -soname ${L}.so.2 -o shared/${L}.so.2.2.1 \
        --whole-archive LDL/Lib/${L}.a && \
        ln -sf ${L}.so.2.2.1 shared/${L}.so.2 && \
        ln -sf ${L}.so.2.2.1 shared/${L}.so

    L=libcholmod
    ${LD} --hash-style=gnu -shared -soname ${L}.so.3 -o shared/${L}.so.3.0.5 \
        --whole-archive CHOLMOD/Lib/${L}.a -lblas -llapack \
        -L./shared -lamd -lcamd -lcolamd -lccolamd -lsuitesparseconfig -lm && \
        ln -sf ${L}.so.3.0.5 shared/${L}.so.3 && \
        ln -sf ${L}.so.3.0.5 shared/${L}.so

    L=libspqr
    ${LD} --hash-style=gnu -shared -soname ${L}.so.2 -o shared/${L}.so.2.0.1 \
        --whole-archive SPQR/Lib/${L}.a -lblas -llapack \
        -L./shared -lcholmod -lsuitesparseconfig -lm && \
        ln -sf ${L}.so.2.0.1 shared/${L}.so.2 && \
        ln -sf ${L}.so.2.0.1 shared/${L}.so

    L=libcxsparse
    ${LD} --hash-style=gnu -shared -soname ${L}.so.3 -o shared/${L}.so.3.1.4 \
        --whole-archive CXSparse/Lib/${L}.a && \
        ln -sf ${L}.so.3.1.4 shared/${L}.so.3 && \
        ln -sf ${L}.so.3.1.4 shared/${L}.so

    L=libklu
    ${LD} --hash-style=gnu -shared -soname ${L}.so.1 -o shared/${L}.so.1.3.2 \
        --whole-archive KLU/Lib/${L}.a -L./shared -lamd -lbtf \
        -lsuitesparseconfig -lm && \
        ln -sf ${L}.so.1.3.2 shared/${L}.so.1 && \
        ln -sf ${L}.so.1.3.2 shared/${L}.so

    L=libumfpack
    ${LD} --hash-style=gnu -shared -soname ${L}.so.5 -o shared/${L}.so.5.7.1 \
        --whole-archive UMFPACK/Lib/${L}.a -lblas -llapack -L./shared \
        -lamd -lcholmod -lsuitesparseconfig -lm && \
        ln -sf ${L}.so.5.7.1 shared/${L}.so.5 && \
        ln -sf ${L}.so.5.7.1 shared/${L}.so
}

do_install() {
    install -dm755 ${D}/usr/lib
    install -dm755 ${D}/usr/include

    for _lib in SuiteSparse_config AMD CAMD COLAMD BTF KLU LDL CCOLAMD \
        UMFPACK CHOLMOD CXSparse SPQR; do
        make -C ${_lib} INSTALL_LIB=${D}/usr/lib \
            INSTALL_INCLUDE=${D}/usr/include install
    done

    rm -f ${D}/usr/lib/*.a
    cp -d shared/*.so* ${D}/usr/lib/
}
