SUMMARY = "qrupdate"
DESCRIPTION = "Fortran library for fast updates of QR and Cholesky decompositions"
HOMEPAGE = "http://sourceforge.net/projects/qrupdate"
LICENSE = "GPLv3"
DEPENDS = "gcc-runtime libgfortran openblas-lapack"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

PR = "r1"
SRC_URI = " \
    http://downloads.sourceforge.net/project/${PN}/${PN}/1.2/${PN}-${PV}.tar.gz \
    file://0001-use-global-FC-var.patch \
"

LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}-${PV}/COPYING;md5=d32239bcb673463ab874e80d47fae504"
SRC_URI[md5sum] = "6d073887c6e858c24aeda5b54c57a8c4"
SRC_URI[sha256sum] = "e2a1c711dc8ebc418e21195833814cb2f84b878b90a2774365f0166402308e08"

do_compile() {
    make F77='${FC}' solib
}

do_install() {
    make PREFIX=/usr DESTDIR=${D} install
}
