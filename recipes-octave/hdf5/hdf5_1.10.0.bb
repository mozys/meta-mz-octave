SUMMERY = "hdf5"
DESCRIPTION = "General purpose library and file format for storing scientific data"
HOMEPAGE = "http://www.hdfgroup.org/HDF5/"
LICENSE = "CLOSED"
DEPENDS = "gcc-runtime libgfortran"

SRC_URI = " \
    ftp://ftp.hdfgroup.org/HDF5/releases/${PN}-1.10/${PN}-${PV}/src/${PN}-${PV}.tar.bz2 \
    file://0001-disable-H5detect-H5make_libsettings-use-local-ones.patch \
    file://0002-add-prebuilt-H5Tinit-Phytec-Mira-i.mx6-Quad.patch \
    file://0003-add-dummy-H5lib_settings.patch \
    file://TryRunResults.cmake \
"

LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}-${PV}/COPYING;md5=1fe675a1615def15caca141e32817a5a"
SRC_URI[md5sum] = "355243bda5df386aea25f079d550947b"
SRC_URI[sha256sum] = "31ff70dc7c7317066ab3bda3eec4498a8b099c69c1271b008ed3df388e743d28"

EXTRA_OECMAKE += " \
    -C${WORKDIR}/TryRunResults.cmake \
"

do_compile_prepend() {
    HS=${WORKDIR}/${PN}-${PV}/src/H5lib_settings.c
    echo "Hallo Tom"
    echo ${HS}

    echo "char H5libhdf5_settings[]=" >${HS}
    sed "s/\(.*\)/\t\"\1\\\n\"/" ${WORKDIR}/build/libhdf5.settings >>${HS}
    echo ";" >>${HS}
    echo "Hallo Tim"
}

do_install_append() {
    # remove unnecessary files

    ln -sf libhdf5-shared.so.100.0.0 ${D}/${libdir}/libhdf5.so
    rm -f ${D}/${datadir}/{COPYING,RELEASE.txt,USING_HDF5_CMake.txt}
}

FILES_${PN} += "${libdir}/libhdf5.settings"
FILES_${PN}-dev += "${datadir}/cmake/*"
FILES_${PN}-drop += "${datadir}/COPYING ${datadir}/RELEASE.txt \
        ${datadir}/USING_HDF5_CMake.txt"

inherit cmake
