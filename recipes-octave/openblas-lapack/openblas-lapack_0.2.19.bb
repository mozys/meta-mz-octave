SUMMARY = "openblas-lapack"
DESCRIPTION = "Complete LAPACK and BLAS implementation using optimised OpenBLAS routines"
HOMEPAGE = "http://www.gnu.org/software/octave/"
LICENSE = "CLOSED"
DEPENDS = "gcc-runtime libgfortran"

PROVIDES = "blas lapack"
RPROVIDES_${PN} = "blas lapack"

PR = "r1"
S = "${WORKDIR}/OpenBLAS-${PV}"
SRC_URI = "\
    https://github.com/xianyi/OpenBLAS/archive/v${PV}.tar.gz \
"

LIC_FILES_CHKSUM = "file://${S}/LICENSE;md5=5adf4792c949a00013ce25d476a2abc0"
SRC_URI[md5sum] = "28c998054fd377279741c6f0b9ea7941"
SRC_URI[sha256sum] = "9c40b5e4970f27c5f6911cb0a28aa26b6c83f17418b69f8e5a116bb983ca8557"

do_configure[noexec] = "1"

_makeopts = " \
    TARGET=ARMV7 HOSTCC=gcc \
    CROSS_SUFFIX=${TARGET_PREFIX} \
    USE_OPENMP=0 USE_THREAD=1 \
    NO_LAPACK=0 BUILD_LAPACK_DEPRECATED=1 \
"

do_compile() {
    make ${_makeopts} libs netlib shared
}

do_install() {
    make ${_makeopts} PREFIX=/usr DESTDIR=${D} install

    # BLAS
    ln -sf libopenblas.so ${D}/${libdir}/libblas.so
    ln -sf libopenblas.so ${D}/${libdir}/libblas.so.3
    ln -sf libopenblas.so ${D}/${libdir}/libblas.so.3.6.0
    # CBLAS
    ln -sf libopenblas.so ${D}/${libdir}/libcblas.so
    # LAPACK
    ln -sf libopenblas.so ${D}/${libdir}/liblapack.so
    ln -sf libopenblas.so ${D}/${libdir}/liblapack.so.3
    ln -sf libopenblas.so ${D}/${libdir}/liblapack.so.3.6.0
    # LAPACKE
    ln -sf libopenblas.so ${D}/${libdir}/liblapacke.so
}
FILES_${PN} += "${bindir} ${libdir}/*.so*"
FILES_${PN}-dev = "${libdir}/cmake/* ${includedir}"

FILES_SOLIBSDEV = ""
SOLIBS = ".so .so.3 .so.3.6.0"
INSANE_SKIP_${PN} += "dev-so"
