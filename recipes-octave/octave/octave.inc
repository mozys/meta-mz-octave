DESCRIPTION = "Matlab alternative for numerical computations"
HOMEPAGE = "http://www.gnu.org/software/octave/"
SECTION = "console/scientific"
LICENSE = "GPLv2"
DEPENDS = "readline ncurses fftw gperf-native openblas-lapack libpcre qhull gcc-runtime libgfortran zlib curl fontconfig freetype llvm3.3 imagemagick arpack-ng qrupdate suitesparse virtual/libgl gl2ps glpk hdf5 libsndfile1 gnuplot"
RDEPENDS_${PN} = "libfftw3 gnuplot"
# fftw compiled with --disable-fortran
PROVIDES = "octave"

ARM_INSTRUCTION_SET = "arm"

TARGET_CC_ARCH += "${LDFLAGS}"

inherit autotools

# adapt F77 for your application, also, adapt configure for your application

EXTRA_OECONF = " \
    F77='${FC}' \
    --with-x=no --with-opengl=yes \
    --without-framework-carbon \
    gl_cv_func_gettimeofday_clobber=no \
"

do_compile_prepend () {
    mkdir -p liboctave/operators
    mkdir -p liboctave/numeric

    mkdir -p libinterp/corefcn
    mkdir -p libinterp/operators
    mkdir -p libinterp/octave-value
    mkdir -p libinterp/parse-tree
}

PACKAGES += "octave-m"

# octave-m provides matlab routines in .m file format
FILES_${PN}-m = "${datadir}/${PN}/*"
