require octave.inc

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

PR = "r1"
SRC_URI = " \
    ftp://ftp.gnu.org/gnu/octave/${PN}-${PV}.tar.gz \
    file://0001-configure-check-return-code-of-pcre-config.patch \
"

SRC_URI[md5sum] = "941bb1fc981a4abf58ae9e438bb13dd1"
SRC_URI[sha256sum] = "5a16a42fca637ae1b55b4a5a6e7b16a6df590cbaeeb4881a19c7837789515ec6"

LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}-${PV}/COPYING;md5=d32239bcb673463ab874e80d47fae504"

PACKAGES =+ "libcruft liboctave liboctinterp \
       libcruft-dev liboctave-dev liboctinterp-dev \
             libcruft-dbg liboctave-dbg liboctinterp-dbg octave-oct"

FILES_libcruft-dev = "${libdir}/${PN}/${PV}/libcruft.so"
FILES_libcruft-dbg += "${libdir}/${PN}/${PV}/.debug/libcruft*"

FILES_liboctave-dev = "${libdir}/${PN}/${PV}/liboctave.so"
FILES_liboctave-dbg += "${libdir}/${PN}/${PV}/.debug/liboctave*"

FILES_liboctinterp-dev = "${libdir}/${PN}/${PV}/liboctinterp.so"
FILES_liboctinterp-dbg += "${libdir}/${PN}/${PV}/.debug/liboctinterp*"

# ocatave shared files (icons, appdata)
FILES_${PN} += "${datadir}"

# octave-oct provides subroutines in .oct file format
FILES_${PN}-oct = "${libexecdir}/${PN}/${PV}/oct/${TARGET_SYS}/.debug/*.oct"

# debug files
FILES_${PN}-dbg += "${libdir}/${PN}/${PV}/oct/${TARGET_SYS}/.debug"
