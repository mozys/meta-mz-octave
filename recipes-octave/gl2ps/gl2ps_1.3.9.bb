SUMMERY = "gl2ps"
DESCRIPTION = "an OpenGL to PostScript printing library"
HOMEPAGE = "http://geuz.org/gl2ps/"
LICENSE = "LGPL"
DEPENDS = "gcc-runtime virtual/libgl"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

S = "${WORKDIR}/${PN}-${PV}-source"
SRC_URI = " \
    http://geuz.org/${PN}/src/${PN}-${PV}.tgz \
    file://0001-enable-gl.patch \
"

LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}-${PV}-source/COPYING.GL2PS;md5=6b1349ed40ea31cc7560a03acc029455"
SRC_URI[md5sum] = "377b2bcad62d528e7096e76358f41140"
SRC_URI[sha256sum] = "8a680bff120df8bcd78afac276cdc38041fed617f2721bade01213362bcc3640"

EXTRA_OECMAKE += " \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_EXE_LINKER_FLAGS=-lm \
    -DOPENGL_FOUND=1 \
"

inherit cmake
