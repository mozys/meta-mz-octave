SUMMERY = "glpk"
DESCRIPTION = "GNU Linear Programming Kit : solve LP, MIP and other problems."
HOMEPAGE = "http://www.gnu.org/software/glpk/glpk.html"
LICENSE = "GPLv3"
DEPENDS = "gmp"

SRC_URI = " \
    http://ftp.gnu.org/gnu/glpk/${PN}-${PV}.tar.gz \
"

LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}-${PV}/COPYING;md5=d32239bcb673463ab874e80d47fae504"
SRC_URI[md5sum] = "eda7965907f6919ffc69801646f13c3e"
SRC_URI[sha256sum] = "1356620cb0a0d33ac3411dd49d9fd40d53ece73eaec8f6b8d19a77887ff5e297"

inherit autotools
