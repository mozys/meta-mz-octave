SUMMARY = "arpack-ng"
DESCRIPTION = "Collection of Fortran77 subroutines designed to solve large scale eigenvalue problems."
HOMEPAGE = "https://forge.scilab.org/index.php/p/arpack-ng/"
LICENSE = "BSD"
DEPENDS = "gcc-runtime libgfortran openblas-lapack"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

S = "${WORKDIR}/git"
SRC_URI = " \
    git://github.com/opencollab/${PN};tag=${PV};protocol=http \
    file://0001_cmake__add_install_targets.patch \
"

LIC_FILES_CHKSUM = "file://${WORKDIR}/git/COPYING;md5=29e47344f98423df9aa3032994f313fc"

EXTRA_OECMAKE += " \
    -DBUILD_SHARED_LIBS=ON \
"

inherit cmake

FILES_${PN} += "${bindir} ${libdir}/*.so*"
FILES_${PN}-dev = "${libdir}/cmake/* ${includedir}"

FILES_SOLIBSDEV = ""
SOLIBS = ".so"
INSANE_SKIP_${PN} += "dev-so"
