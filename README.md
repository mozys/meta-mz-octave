build/bblayers.conf
---
add:

	BBLAYERS += " \
	  ${OEROOT}/../meta-mz-octave \
	"

build/conf/local.conf
---
replace:

	 #  - Uncomment for Freescale i.MX6 proprietary GPU libraries
	-#LICENSE_FLAGS_WHITELIST += "license-freescale_v6-february-2015_imx-gpu-viv"
	+LICENSE_FLAGS_WHITELIST += "license-freescale_v6-february-2015_imx-gpu-viv"
	 #  - Uncomment for Freescale i.MX6 legacy VPU firmware blobs
	-#LICENSE_FLAGS_WHITELIST += "license-freescale_v6-february-2015_firmware-imx"
	+LICENSE_FLAGS_WHITELIST += "license-freescale_v6-february-2015_firmware-imx"

add:

	# Enable fortran
	FORTRAN_forcevariable = ",fortran"
